package com.edreams.application

import android.content.Context
import android.content.pm.PackageManager
import android.support.multidex.MultiDexApplication

import com.edreams.util.SharedPrefs

class App : MultiDexApplication() {

    companion object {

        lateinit var appContext: Context

        fun versionName(): String = try {

            appContext.packageManager
                    .getPackageInfo(appContext.packageName, 0)
                    .versionName

        } catch (e1: PackageManager.NameNotFoundException) {

            ""
        }
    }

    override fun onCreate() {

        super.onCreate()

        appContext = this

        initialiseSharedPreferences()
    }

    private fun initialiseSharedPreferences() {

        SharedPrefs.initialise(this)
    }
}
