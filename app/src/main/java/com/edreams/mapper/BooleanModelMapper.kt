package com.edreams.mapper


import com.edreams.cache.model.BooleanRealmModel

object BooleanModelMapper : IModelMapper<Boolean, Boolean, BooleanRealmModel> {

    override fun toBusinessModelFromBackend(model: Boolean): Boolean {

        return model
    }

    override fun toBusinessModelFromCache(model: BooleanRealmModel): Boolean {

        return model.result
    }

    override fun toCacheModelFromBusiness(model: Boolean): BooleanRealmModel {

        return BooleanRealmModel(model)
    }

    override fun toCacheModelFromBackend(model: Boolean): BooleanRealmModel {

        return BooleanRealmModel(model)
    }
}
