package com.edreams.mapper


import com.edreams.backend.model.CurrencyBackendModel
import com.edreams.business.model.CurrencyBusinessModel

import io.realm.RealmObject

object CurrencyModelMapper : IModelMapper<CurrencyBusinessModel, CurrencyBackendModel, RealmObject> {

    override fun toBusinessModelFromBackend(model: CurrencyBackendModel): CurrencyBusinessModel {

        return CurrencyBusinessModel(model)
    }

    override fun toBusinessModelFromCache(model: RealmObject): CurrencyBusinessModel {

        return CurrencyBusinessModel()
    }

    override fun toCacheModelFromBusiness(model: CurrencyBusinessModel): RealmObject {

        TODO("not implemented")
    }

    override fun toCacheModelFromBackend(model: CurrencyBackendModel): RealmObject {

        TODO("not implemented")
    }
}
