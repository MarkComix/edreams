package com.edreams.mapper

import com.edreams.config.Constants

object MapperManager {

    fun getMapper(coreKey: String): IModelMapper<*, *, *>? {

        return when (coreKey) {

            Constants.CoreKeys.GET_FLIGHT_LIST -> FlightListModelMapper

            Constants.CoreKeys.GET_CURRENCY -> CurrencyModelMapper

            Constants.CoreKeys.SAVE_FLIGHT_LIST -> BooleanModelMapper

            else -> null
        }
    }
}
