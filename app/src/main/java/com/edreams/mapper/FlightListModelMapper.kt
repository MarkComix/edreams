package com.edreams.mapper


import com.edreams.backend.model.FlightBackendModel
import com.edreams.business.model.FlightBusinessModel
import com.edreams.cache.model.FlightRealmModel
import java.util.*

object FlightListModelMapper : IModelMapper<List<FlightBusinessModel>, List<FlightBackendModel>, List<FlightRealmModel>> {

    override fun toBusinessModelFromBackend(model: List<FlightBackendModel>): List<FlightBusinessModel> {

        val retModelList = ArrayList<FlightBusinessModel>()

        model.forEach { item ->
            retModelList.add(FlightBusinessModel(item))
        }

        return retModelList
    }

    override fun toBusinessModelFromCache(model: List<FlightRealmModel>): List<FlightBusinessModel> {

        val retModelList = ArrayList<FlightBusinessModel>()

        model.forEach { item ->
            retModelList.add(FlightBusinessModel(item))
        }

        return retModelList
    }

    override fun toCacheModelFromBusiness(model: List<FlightBusinessModel>): List<FlightRealmModel> {

        val retModelList = ArrayList<FlightRealmModel>()

        model.forEach { item ->
            retModelList.add(FlightRealmModel(item))
        }

        return retModelList
    }

    override fun toCacheModelFromBackend(model: List<FlightBackendModel>): List<FlightRealmModel> {

        val retModelList = ArrayList<FlightRealmModel>()

        model.forEach { item ->
            retModelList.add(FlightRealmModel(item))
        }

        return retModelList
    }
}
