package com.edreams.mapper

interface IModelMapper<BusinessModel, BackendModel, RealmObject> {

    fun toBusinessModelFromBackend(model: BackendModel): BusinessModel

    fun toBusinessModelFromCache(model: RealmObject): BusinessModel

    fun toCacheModelFromBusiness(model: BusinessModel): RealmObject

    fun toCacheModelFromBackend(model: BackendModel): RealmObject
}
