package com.edreams.business.resolver;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

@Retention(SOURCE)
@StringDef({
        Strategy.ONLINE,
        Strategy.OFFLINE,
        Strategy.CACHED
})
public @interface Strategy {

    /**
     * Interacts only with the backend.
     */
    String ONLINE = "ONLINE";

    /**
     * Interacts only with the local database.
     */
    String OFFLINE = "OFFLINE";

    /**
     * Interacts with both the local database and the backend.
     */
    String CACHED = "CACHED";
}


