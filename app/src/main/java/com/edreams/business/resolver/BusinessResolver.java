package com.edreams.business.resolver;

import java.util.Map;

import rx.Observable;

public class BusinessResolver implements IBusinessResolver {

    private IBusinessResolver businessResolver;

    public BusinessResolver(@Strategy String strategy, String coreKey) {

        switch (strategy) {

            case Strategy.ONLINE: {

                businessResolver = new OnlineBusinessResolver(coreKey);

                break;
            }

            case Strategy.OFFLINE: {

                businessResolver = null; //TODO Not implemented in this Test App

                break;
            }

            case Strategy.CACHED: {

                businessResolver =new CachedBusinessResolver(coreKey);

                break;
            }

            default:
                throw new IllegalArgumentException("Strategy not allowed");
        }
    }

    @Override
    public Observable getObservable(Map<String, Object> params) {

        return businessResolver.getObservable(params);
    }
}
