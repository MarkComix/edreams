package com.edreams.business.resolver;

import rx.Subscriber;

class CachedBusinessResolver extends BaseBusinessResolver {

    public CachedBusinessResolver(String coreKey) {

        super(coreKey);
    }

    protected void resolve() {

        // noinspection unchecked
        cacheDAO.excecuteQuery(params).subscribe(new Subscriber() {

                 @Override
                 public void onCompleted() {

                 }

                 @Override
                 public void onError(Throwable e) {

                     try {

                         onFailureGetItem(e);

                     } catch (Exception e1) {

                         e1.printStackTrace();
                     }
                 }

                 @Override
                 public void onNext(Object entity) {

                     onSuccessGetItem(entity);
                 }
             }
        );
    }

    @Override
    protected void onSuccessGetItem(Object entity) {

        Object model = mapper.toBusinessModelFromCache(entity);

        emitModel(model);
    }
}

