package com.edreams.business.resolver;

import android.util.ArrayMap;
import android.util.Log;

import com.edreams.backend.BackendManager;
import com.edreams.backend.dao.IBackendDAO;
import com.edreams.backend.model.CurrencyBackendModel;
import com.edreams.business.model.FlightBusinessModel;
import com.edreams.cache.CacheManager;
import com.edreams.cache.dao.ICacheDAO;
import com.edreams.cache.model.FlightRealmModel;
import com.edreams.config.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscriber;

public class GetFlightListFromCacheBusinessResolver extends CachedBusinessResolver {

    private int backendCallCount;
    private HashMap<String, Float> currencyHash;
    private List<FlightBusinessModel> businessFlightList;

    public GetFlightListFromCacheBusinessResolver() {

        //Get the Flight List from the Cache using Parent Logic
        super(Constants.CoreKeys.GET_FLIGHT_LIST);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onSuccessGetItem(Object flightList) {

        Log.i(Constants.APP_TAG, "Get Flight List from Cache Finish Successfully");

        if (((List<FlightRealmModel>) flightList).isEmpty()) {

            emitModel(new ArrayList<FlightBusinessModel>());

        } else {

            //Keep Flight to used later
            businessFlightList = (List<FlightBusinessModel>)mapper.toBusinessModelFromCache(flightList);

            updateEuroPrice();
        }
    }

    private void updateEuroPrice(){

        //After Get the List, I need the different Currencies from the Flight List
        //So I get the list from Cache using the same Flight Filters
        ICacheDAO cacheDAO = CacheManager.INSTANCE.getItemDAO(Constants.CoreKeys.GET_FLIGHT_CURRENCY_LIST);
        cacheDAO.excecuteQuery(params).subscribe(new Subscriber<Object>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                try {

                    onFailureGetItem(e);

                } catch (Exception e1) {

                    e1.printStackTrace();
                }

                Log.i(Constants.APP_TAG, "Get Flights Currency List from Cache Fail");
            }

            @Override
            public void onNext(Object currencyList) {

                Log.i(Constants.APP_TAG, "Get Flights Currency List from Cache Finish Successfully");

                //After getting the Dif Currency List, I need to call backend and get the current
                //Exchange Rate, so I have to iterate and make the calls

                currencyHash = new HashMap<>();

                backendCallCount = 0;

                //Make a Backend call for each Currency
                for (FlightRealmModel realmModel: (List<FlightRealmModel>)currencyList) {

                    getCurrencyExchange(realmModel.getCurrency());
                }
            }
        });
    }

    private void getCurrencyExchange(String currencyFrom){

        //Load data in Params
        final Map<String, Object> currencyParams = new ArrayMap<>();

        currencyParams.put(Constants.Params.CURRENCY_FROM, currencyFrom);
        currencyParams.put(Constants.Params.CURRENCY_TO, Constants.CurrencySymbols.EUR);

        //Call backend
        IBackendDAO backendDAO = BackendManager.INSTANCE.getBackendDAO(Constants.CoreKeys.GET_CURRENCY);

        //Keep call count
        backendCallCount++;

        // noinspection unchecked
        backendDAO.makeCall(currencyParams).subscribe(new Subscriber() {

              @Override
              public void onCompleted() {

              }

              @Override
              public void onError(Throwable e) {

                  try {

                      onFailureGetItem(e);

                      //On call Finish
                      backendCallCount--;

                  } catch (Exception e1) {

                      e1.printStackTrace();
                  }

                  Log.i(Constants.APP_TAG, "Get Currency Exchange from Backend Fail");
              }

              @Override
              public void onNext(Object currencyModel) {

                  Log.i(Constants.APP_TAG, "Get Currency Exchange from Backend Finish Successfully");

                  //One call finish
                  backendCallCount--;

                  //Save the Exchange Rate in a Hash to use it later
                  currencyHash.put(String.valueOf(currencyParams.get(Constants.Params.CURRENCY_FROM)),
                          Float.valueOf(((CurrencyBackendModel)currencyModel).getExchangeRate()));

                  //Check if all backend call finish
                  if (backendCallCount == 0){

                      List<FlightBusinessModel> retList = new ArrayList<>();

                      //Iterate flights and update the Price
                      for (FlightBusinessModel businessModel: businessFlightList) {

                          //Get the Exchange Rate for the Flight Currency
                          if (currencyHash.containsKey(businessModel.getCurrency())){

                              businessModel.setPriceInEuro(businessModel.getPrice() *
                                      currencyHash.get(businessModel.getCurrency()));
                          }

                          //If I have Price Filter, because the filter is in Euros, I have to
                          //apply the filter here
                          if (params.containsKey(Constants.Params.PRICE_FROM)){

                              float priceFrom = Float.parseFloat((String)params.get(Constants.Params.PRICE_FROM));
                              float priceTo = Float.parseFloat((String)params.get(Constants.Params.PRICE_TO));

                              if (businessModel.getPriceInEuro() >= priceFrom && businessModel.getPriceInEuro() <= priceTo)
                                  retList.add(businessModel);

                          }else{

                              retList.add(businessModel);
                          }
                      }

                      //All is done, I can send the list to UI
                      emitModel(retList);
                  }
              }
          }
        );
    }
}
