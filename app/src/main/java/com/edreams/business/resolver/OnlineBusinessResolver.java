package com.edreams.business.resolver;

import rx.Subscriber;

class OnlineBusinessResolver extends BaseBusinessResolver {

    public OnlineBusinessResolver(String coreKey) {

        super(coreKey);
    }

    protected void resolve() {

        // noinspection unchecked
        backendDAO.makeCall(params).subscribe(new Subscriber() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                    try {

                        onFailureGetItem(e);

                    } catch (Exception e1) {

                        e1.printStackTrace();
                    }
                }

                @Override
                public void onNext(Object entity) {

                    onSuccessGetItem(entity);
                }
            }
        );
    }

}
