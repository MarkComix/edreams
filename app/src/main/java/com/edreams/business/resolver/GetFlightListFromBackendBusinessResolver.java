package com.edreams.business.resolver;

import android.util.ArrayMap;
import android.util.Log;

import com.edreams.cache.model.FlightRealmModel;
import com.edreams.config.Constants;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

public class GetFlightListFromBackendBusinessResolver extends OnlineBusinessResolver {

    public GetFlightListFromBackendBusinessResolver() {

        //Get the Full List from Backend (1000 records)
        super(Constants.CoreKeys.GET_FLIGHT_LIST);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onSuccessGetItem(Object backendFlightList) {

        //Convert from Backend to Cache Model
        List<FlightRealmModel> cacheFlightList = (List<FlightRealmModel>) mapper.toCacheModelFromBackend(backendFlightList);

        //Add List to Params
        Map<String, Object> paramsToSave = new ArrayMap<>();
        paramsToSave.put(Constants.Params.FLIGHT_LIST, cacheFlightList);

        //Call Resolver to Save Full List in Local Cache
        CachedBusinessResolver resolver = new CachedBusinessResolver(Constants.CoreKeys.SAVE_FLIGHT_LIST);
        resolver.getObservable(paramsToSave).subscribe(new Subscriber() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                try {

                    onFailureGetItem(e);

                } catch (Exception e1) {

                    e1.printStackTrace();
                }

                Log.i(Constants.APP_TAG, "Save Flight List in Cache Fail");

            }

            @Override
            public void onNext(Object successSaveModel) {

                Log.i(Constants.APP_TAG, "Save Flight List in Cache Finish Successfully");

                //Get the Filtered List from Cache
                getFlightListFromCache();
            }
        });
    }

    private void getFlightListFromCache(){

        //Call the Resolver to get the Filtered List from Cache
        GetFlightListFromCacheBusinessResolver resolver = new GetFlightListFromCacheBusinessResolver();
        resolver.getObservable(params).subscribe(new Subscriber() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                try {

                    onFailureGetItem(e);

                } catch (Exception e1) {

                    e1.printStackTrace();
                }

                Log.i(Constants.APP_TAG, "Get Flight List from Cache Fail");

            }

            @Override
            public void onNext(Object businessFlightList) {

                Log.i(Constants.APP_TAG, "Get Flight List from Cache Finish Successfully");

                //Emit the Filtered List
                emitModel(businessFlightList);
            }
        });
    }
}
