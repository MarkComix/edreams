package com.edreams.business.resolver;

import java.util.Map;

import rx.Observable;

interface IBusinessResolver {

    Observable getObservable(final Map<String, Object> params);
}
