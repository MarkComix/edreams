package com.edreams.business.resolver;

import com.edreams.backend.BackendManager;
import com.edreams.backend.dao.IBackendDAO;
import com.edreams.cache.CacheManager;
import com.edreams.cache.dao.ICacheDAO;
import com.edreams.mapper.IModelMapper;
import com.edreams.mapper.MapperManager;

import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;

class BaseBusinessResolver implements IBusinessResolver {

    protected Subscriber subscriber;

    protected Map<String, Object> params;

    protected IBackendDAO backendDAO;

    protected IModelMapper mapper;

    protected ICacheDAO cacheDAO;

    public BaseBusinessResolver(String coreKey) {

        this.backendDAO = BackendManager.INSTANCE.getBackendDAO(coreKey);

        this.mapper = MapperManager.INSTANCE.getMapper(coreKey);

        this.cacheDAO = CacheManager.INSTANCE.getItemDAO(coreKey);
    }

    @Override
    public Observable getObservable(final Map<String, Object> params) {

        return Observable.create(
                new Observable.OnSubscribe<List>() {

                    @Override
                    public void call(Subscriber<? super List> subscriber) {

                        BaseBusinessResolver.this.subscriber = subscriber;

                        BaseBusinessResolver.this.params = params;

                        resolve();
                    }
                }
        );
    }

    protected void resolve() {

    }

    protected void onFailureGetItem(Throwable e) {

        if (subscriber.isUnsubscribed())
            return;

        subscriber.onError(e);
    }

    @SuppressWarnings("unchecked")
    protected void onSuccessGetItem(Object entity) {

        Object model = mapper.toBusinessModelFromBackend(entity);

        emitModel(model);
    }

    protected void emitModel(Object model) {

        if (subscriber.isUnsubscribed())
            return;

        subscriber.onNext(model);
    }
}
