package com.edreams.business

import com.edreams.business.resolver.GetFlightListFromBackendBusinessResolver
import com.edreams.business.resolver.GetFlightListFromCacheBusinessResolver
import rx.Observable
import rx.Subscriber

object BusinessManager {

    internal lateinit var subscriber: Subscriber<Any>

    fun getFlightList(params: Map<String, Any>, forceBackendCall: Boolean): Observable<Any> {

        return if (forceBackendCall)
            this.getFlightListFromBackend(params)
        else
            this.getFlightListFromCache(params)
    }

    private fun getFlightListFromBackend(params: Map<String, Any>): Observable<Any> {

        val coreResolver = GetFlightListFromBackendBusinessResolver()

        return coreResolver.getObservable(params)
    }


    private fun getFlightListFromCache(params: Map<String, Any>): Observable<Any> {

        val coreResolver = GetFlightListFromCacheBusinessResolver()

        return coreResolver.getObservable(params)
    }

}