package com.edreams.business.model

import com.edreams.backend.model.FlightBackendModel
import com.edreams.cache.model.FlightRealmModel

class FlightBusinessModel(var inbound: FlightDataBusinessModel = FlightDataBusinessModel(),
                          var outbound: FlightDataBusinessModel = FlightDataBusinessModel(),
                          var price: Float = 0f,
                          var currency: String = "",
                          var priceInEuro: Float = 0f) : BaseBusinessModel() {


    constructor(backendModel: FlightBackendModel) : this(FlightDataBusinessModel(backendModel.inbound),
                                                    FlightDataBusinessModel(backendModel.outbound),
                                                    java.lang.Float.parseFloat(backendModel.price),
                                                    backendModel.currency,
                                                    0f)

    constructor(realmModel: FlightRealmModel) : this(FlightDataBusinessModel(realmModel.inbound),
                                                    FlightDataBusinessModel(realmModel.outbound),
                                                    realmModel.price,
                                                    realmModel.currency,
                                          0f)

}
