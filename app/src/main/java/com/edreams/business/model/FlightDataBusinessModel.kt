package com.edreams.business.model

import com.edreams.backend.model.FlightDataBackendModel
import com.edreams.cache.model.FlightDataRealmModel

class FlightDataBusinessModel(var airline: String = "",
                              var airlineImage: String = "",
                              var destination: String = "",
                              var origin: String = "") : BaseBusinessModel() {

    constructor(backendModel: FlightDataBackendModel) : this(backendModel.airline,
                                                            backendModel.airlineImage,
                                                            backendModel.destination,
                                                            backendModel.origin)

    constructor(realmModel: FlightDataRealmModel) : this(realmModel.airline,
                                                        realmModel.airlineImage,
                                                        realmModel.destination,
                                                        realmModel.origin)
}
