package com.edreams.business.model

import com.edreams.backend.model.CurrencyBackendModel
import com.edreams.backend.response.BaseResponse

data class CurrencyBusinessModel (var currency: String = "",
                                  var exchangeRate: String = "0") : BaseResponse() {

    constructor(backendModel: CurrencyBackendModel) : this(backendModel.currency,
                                                            backendModel.exchangeRate)
}