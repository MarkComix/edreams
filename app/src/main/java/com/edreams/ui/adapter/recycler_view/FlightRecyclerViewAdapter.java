package com.edreams.ui.adapter.recycler_view;

import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.edreams.R;
import com.edreams.ui.adapter.recycler_view.view_holder.BaseViewHolder;
import com.edreams.ui.adapter.recycler_view.view_holder.FlightViewHolder;
import com.edreams.ui.model.FlightUIModel;

public class FlightRecyclerViewAdapter extends BaseRecyclerViewAdapter {

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_flight, parent, false);

        return new FlightViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final FlightViewHolder realHolder = (FlightViewHolder) holder;
        final FlightUIModel realItem = (FlightUIModel)items.get(position);

        if (realItem != null) {

            //Inbound Airline Image
            if (!TextUtils.isEmpty(realItem.getInbound().getAirlineImage())){

                Glide.with(holder.itemView.getContext())
                        .load(realItem.getInbound().getAirlineImage())
                        .placeholder(AppCompatResources.getDrawable(holder.itemView.getContext(),
                                R.drawable.empty_placeholder_airline))
                        .dontAnimate()
                        .into(realHolder.imageViewInboundAirlineImage);
            }

            //Inbound Airline Name
            if (!TextUtils.isEmpty(realItem.getInbound().getAirline()))
                realHolder.textViewInboundAirlineName.setText(realItem.getInbound().getAirline());

            //Inbound Origin
            if (!TextUtils.isEmpty(realItem.getInbound().getOrigin()))
                realHolder.textViewInboundOrigin.setText(realItem.getInbound().getOrigin());

            //Inbound Destination
            if (!TextUtils.isEmpty(realItem.getInbound().getDestination()))
                realHolder.textViewInboundDestination.setText(realItem.getInbound().getDestination());

            //Outbound Airline Image
            if (!TextUtils.isEmpty(realItem.getOutbound().getAirlineImage())) {

                Glide.with(holder.itemView.getContext())
                        .load(realItem.getOutbound().getAirlineImage())
                        .placeholder(AppCompatResources.getDrawable(holder.itemView.getContext(),
                                R.drawable.empty_placeholder_airline))
                        .dontAnimate()
                        .into(realHolder.imageViewOutboundAirlineImage);
            }

            //Outbound Airline Name
            if (!TextUtils.isEmpty(realItem.getOutbound().getAirline()))
                realHolder.textViewOutboundAirlineName.setText(realItem.getOutbound().getAirline());

            //Inbound Origin
            if (!TextUtils.isEmpty(realItem.getOutbound().getOrigin()))
                realHolder.textViewOutboundOrigin.setText(realItem.getOutbound().getOrigin());

            //Inbound Destination
            if (!TextUtils.isEmpty(realItem.getOutbound().getDestination()))
                realHolder.textViewOutboundDestination.setText(realItem.getOutbound().getDestination());

            //Price
            if (!TextUtils.isEmpty(realItem.getCurrency()) && !TextUtils.isEmpty(realItem.getPrice()) )
                realHolder.textViewPrice.setText(String.format("%s %s", realItem.getCurrency(), realItem.getPrice()));

        }
    }
}
