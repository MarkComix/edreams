package com.edreams.ui.adapter.recycler_view.view_holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.edreams.R;

import butterknife.BindView;

public class FlightViewHolder extends BaseViewHolder {

    @BindView(R.id.list_item_flight_airline_inbound_image)
    public ImageView imageViewInboundAirlineImage;

    @BindView(R.id.list_item_flight_airline_inbound_name)
    public TextView textViewInboundAirlineName;

    @BindView(R.id.list_item_flight_airline_outbound_image)
    public ImageView imageViewOutboundAirlineImage;

    @BindView(R.id.list_item_flight_airline_outbound_name)
    public TextView textViewOutboundAirlineName;

    @BindView(R.id.list_item_flight_inbound_origin)
    public TextView textViewInboundOrigin;

    @BindView(R.id.list_item_flight_inbound_destination)
    public TextView textViewInboundDestination;

    @BindView(R.id.list_item_flight_outbound_origin)
    public TextView textViewOutboundOrigin;

    @BindView(R.id.list_item_flight_outbound_destination)
    public TextView textViewOutboundDestination;

    @BindView(R.id.list_item_flight_price)
    public TextView textViewPrice;

    public FlightViewHolder(View itemView) {

        super(itemView);
    }

}
