package com.edreams.ui.adapter.recycler_view;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.edreams.ui.model.BaseUIModel;

import java.util.ArrayList;
import java.util.List;

public class BaseRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected List<BaseUIModel> items;

    public BaseRecyclerViewAdapter() {

        this.items = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {

        return items == null ? 0 : items.size();
    }

    public boolean isEmpty() {

        return getItemCount() == 0;
    }

    public void insertItems(List<BaseUIModel> items) {

        int oldCount = getItemCount();

        this.items = items;

        int newCount = getItemCount();

        notifyItemRangeRemoved(0, oldCount);

        notifyItemRangeInserted(0, newCount);
    }

    public void clear() {

        int oldCount = getItemCount();

        this.items = new ArrayList<>();

        int newCount = 0;

        notifyItemRangeRemoved(0, oldCount);

        notifyItemRangeInserted(0, newCount);
    }
}
