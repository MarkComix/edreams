package com.edreams.ui.adapter.recycler_view.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class BaseViewHolder extends RecyclerView.ViewHolder {

    public BaseViewHolder(View itemView) {

        super(itemView);

        ButterKnife.bind(this, itemView);
    }
}
