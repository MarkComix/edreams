package com.edreams.ui.presenter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.edreams.ui.view.BaseView;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class BasePresenter<View extends BaseView> implements Presenter {

    protected View view;
    protected Fragment fragment;

    private CompositeSubscription subscriptions;

    public BasePresenter() {

        this.subscriptions = new CompositeSubscription();
    }

    public void setView(View view, Fragment fragment) {

        this.view = view;
        this.fragment = fragment;
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {

        cancelAllSubscriptions();

        this.view = null;

        this.fragment = null;
    }

    protected void addSubscription(Subscription subscription) {

        subscriptions.add(subscription);
    }

    protected void cancelSubscription(@Nullable Subscription subscription) {

        if (subscription == null)
            return;

        subscriptions.remove(subscription);
    }

    private void cancelAllSubscriptions() {

        subscriptions.unsubscribe();
    }

    protected void resetSubscriptions() {

        cancelAllSubscriptions();

        subscriptions = new CompositeSubscription();
    }

}
