package com.edreams.ui.presenter

import android.text.TextUtils
import android.util.ArrayMap

import com.edreams.ui.util.FlightListFiltersUtils
import com.edreams.ui.view.FlightListFiltersView

class FlightListFiltersPresenter : BasePresenter<FlightListFiltersView>() {

    private lateinit var selectedFilters: MutableMap<String, String>

    fun initialise(cleanFilters: Boolean) {

        if (!cleanFilters) {

            selectedFilters = FlightListFiltersUtils.getSelectedFiltersValues()

            val origin = selectedFilters[FlightListFiltersUtils.Constants.Labels.ORIGIN] ?: ""
            val destination = selectedFilters[FlightListFiltersUtils.Constants.Labels.DESTINATION] ?: ""
            val priceFrom = selectedFilters[FlightListFiltersUtils.Constants.Labels.PRICE_FROM] ?: ""
            val priceTo = selectedFilters[FlightListFiltersUtils.Constants.Labels.PRICE_TO] ?: ""

            view.loadFilters(origin, destination, priceFrom, priceTo)

        } else {

            FlightListFiltersUtils.clearFilters()
        }
    }

    fun onApplyClick(origin: String, destination: String, priceFrom: String, priceTo: String) {

        if (!TextUtils.isEmpty(priceFrom) && !TextUtils.isEmpty(priceTo)
                && java.lang.Float.valueOf(priceFrom) > java.lang.Float.valueOf(priceTo)) {

            view.showPriceFromBiggerPriceToErrorMessage()

            return
        }

        if (TextUtils.isEmpty(origin) || TextUtils.isEmpty(destination)) {

            view.showOriginAndDestinationAreRequiredErrorMessage()

            return
        }

        selectedFilters = ArrayMap()

        if (!TextUtils.isEmpty(origin))
            selectedFilters[FlightListFiltersUtils.Constants.Labels.ORIGIN] = origin

        if (!TextUtils.isEmpty(destination))
            selectedFilters[FlightListFiltersUtils.Constants.Labels.DESTINATION] = destination

        if (!TextUtils.isEmpty(priceFrom))
            selectedFilters[FlightListFiltersUtils.Constants.Labels.PRICE_FROM] = priceFrom

        if (!TextUtils.isEmpty(priceTo))
            selectedFilters[FlightListFiltersUtils.Constants.Labels.PRICE_TO] = priceTo

        FlightListFiltersUtils.saveSelectedFiltersValues(selectedFilters)

        view.applyFilters()
    }
}
