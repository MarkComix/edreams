package com.edreams.ui.presenter

import android.util.Log

import com.edreams.config.Constants
import com.edreams.ui.view.SplashView

class SplashPresenter : BasePresenter<SplashView>() {

    fun initialise() {

        fragment.view!!.postDelayed({
            try {

                view.navigateToHomeScreen()

            } catch (e: Exception) {

                Log.e(Constants.APP_TAG, "Couldn't navigate to next screen from Splash", e)
            }
        }, STARTUP_DELAY.toLong())
    }

    companion object {

        private const val STARTUP_DELAY = 2000
    }

}
