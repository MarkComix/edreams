package com.edreams.ui.presenter

interface Presenter {

    fun onResume()

    fun onPause()

    fun onDestroy()

}

