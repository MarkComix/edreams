package com.edreams.ui.presenter

import com.edreams.ui.view.BaseView
import rx.Observable
import rx.Subscriber

abstract class BaseRecyclerViewPresenter<View : BaseView> : BasePresenter<View>() {

    fun refreshList() {

        val observable = this.createObservable()

        if (observable != null) {

            addSubscription(observable.subscribe(createSubscriber()))
        }
    }

    protected open fun createObservable(): Observable<*>? {

        return null
    }

    private fun createSubscriber(): Subscriber<Any> {

        return object : Subscriber<Any>() {

            override fun onCompleted() {

            }

            override fun onError(e: Throwable) {

                try {

                    doOnError(e)

                } catch (e1: Exception) {
                    e1.printStackTrace()
                }

            }

            override fun onNext(model: Any) {

                doOnNext(model)
            }
        }
    }

    protected open fun doOnError(e: Throwable) {

    }

    protected open fun doOnNext(model: Any) {

    }
}
