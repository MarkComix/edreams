package com.edreams.ui.presenter

import com.edreams.R
import com.edreams.application.App
import com.edreams.business.BusinessManager
import com.edreams.business.model.FlightBusinessModel
import com.edreams.config.Constants
import com.edreams.ui.model.FlightUIModel
import com.edreams.ui.util.ErrorUtils
import com.edreams.ui.util.FlightListFiltersUtils
import com.edreams.ui.view.FlightListView
import rx.Observable
import java.util.*

class FlightListPresenter : BaseRecyclerViewPresenter<FlightListView>() {

    private lateinit var listParams: MutableMap<String, Any>

    private var forceBackendCall: Boolean = false

    fun initialise() {

        this.listParams = HashMap()

        this.forceBackendCall = true
    }

    override fun createObservable(): Observable<*> {

        this.listParams = HashMap()

        val selectedFilters = FlightListFiltersUtils.getSelectedFiltersValues()

        if (selectedFilters.containsKey(FlightListFiltersUtils.Constants.Labels.ORIGIN))
            this.listParams[Constants.Params.ORIGIN] = selectedFilters[FlightListFiltersUtils.Constants.Labels.ORIGIN] ?: ""

        if (selectedFilters.containsKey(FlightListFiltersUtils.Constants.Labels.DESTINATION))
            this.listParams[Constants.Params.DESTINATION] = selectedFilters[FlightListFiltersUtils.Constants.Labels.DESTINATION] ?: ""

        if (selectedFilters.containsKey(FlightListFiltersUtils.Constants.Labels.PRICE_FROM))
            this.listParams[Constants.Params.PRICE_FROM] = selectedFilters[FlightListFiltersUtils.Constants.Labels.PRICE_FROM] ?: ""

        if (selectedFilters.containsKey(FlightListFiltersUtils.Constants.Labels.PRICE_TO))
            this.listParams[Constants.Params.PRICE_TO] = selectedFilters[FlightListFiltersUtils.Constants.Labels.PRICE_TO] ?: ""

        return BusinessManager.getFlightList(this.listParams, forceBackendCall)
    }

    override fun doOnError(e: Throwable) {

        this.view.showMessage(ErrorUtils.getErrorMsg(e))
    }

    override fun doOnNext(model: Any) {

        val retModelList = ArrayList<FlightUIModel>()

        (model as List<FlightBusinessModel>).forEach { item ->
            retModelList.add(FlightUIModel(item))
        }

        view.showList(retModelList)
    }

    fun getFiltersText() {

        val selectedFilters = FlightListFiltersUtils.getSelectedFiltersValues()

        val selectedFiltersCount = selectedFilters.keys.size

        val filtersText: String

        var showResetButton = true

        if (selectedFiltersCount == 0) {

            filtersText = App.appContext.getString(R.string.view_add_filters_add_filters)

            showResetButton = false

        } else {

            if (selectedFiltersCount == 1) {

                filtersText = App.appContext.getString(R.string.view_add_filters_count_one)

            } else {

                filtersText = App.appContext.getString(R.string.view_add_filters_count, selectedFiltersCount)
            }
        }

        this.view.setFilterButtonAndText(filtersText, showResetButton)
    }

    fun onFiltersClick() {

        this.view.navigateToFiltersScreen()
    }

    fun applyFilters() {

        if (FlightListFiltersUtils.hasFilters()) {

            this.forceBackendCall = false

            this.refreshList()
        }
    }
}
