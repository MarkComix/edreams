package com.edreams.ui.model

import com.edreams.R
import com.edreams.application.App
import com.edreams.business.model.FlightDataBusinessModel

data class FlightDataUIModel(var airline: String = "",
                             var airlineImage: String = "",
                             var destination: String = "",
                             var origin: String = "") : BaseUIModel() {

    constructor(businessModel: FlightDataBusinessModel) : this(businessModel.airline + " " + App.appContext.getString(R.string.airline_label),
                                                            businessModel.airlineImage,
                                                            businessModel.destination,
                                                            businessModel.origin)
}