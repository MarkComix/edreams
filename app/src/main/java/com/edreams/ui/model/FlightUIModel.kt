package com.edreams.ui.model

import com.edreams.business.model.FlightBusinessModel
import com.edreams.config.Constants

data class FlightUIModel(var inbound: FlightDataUIModel = FlightDataUIModel(),
                         var outbound: FlightDataUIModel = FlightDataUIModel(),
                         var price: String = "",
                         var currency: String = "") : BaseUIModel() {

    constructor(businessModel: FlightBusinessModel) : this(FlightDataUIModel(businessModel.inbound),
                                                        FlightDataUIModel(businessModel.outbound),
                                                        String.format("%.2f", businessModel.priceInEuro),
                                                        Constants.CurrencySymbols.EUR)
}
