package com.edreams.ui.view

interface SplashView : BaseView {

    fun navigateToHomeScreen()
}
