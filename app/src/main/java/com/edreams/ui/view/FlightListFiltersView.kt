package com.edreams.ui.view

interface FlightListFiltersView : BaseView {

    fun applyFilters()

    fun loadFilters(origin: String, destination: String, priceFrom: String, priceTo: String)

    fun showPriceFromBiggerPriceToErrorMessage()

    fun showOriginAndDestinationAreRequiredErrorMessage()
}
