package com.edreams.ui.view

interface BaseView {

    fun showMessage(message: String)
}
