package com.edreams.ui.view

interface BaseRecyclerViewView : BaseView {

    fun showList(list: List<*>)
}
