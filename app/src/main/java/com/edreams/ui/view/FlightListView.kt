package com.edreams.ui.view

interface FlightListView : BaseRecyclerViewView {

    fun navigateToFiltersScreen()

    fun setFilterButtonAndText(filtersText: String, showResetButton: Boolean)
}
