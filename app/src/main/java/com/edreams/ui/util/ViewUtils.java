package com.edreams.ui.util;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.edreams.R;
import com.edreams.application.App;

public class ViewUtils {

    public static void displaySnackbar(View view, String message) {

        displaySnackbar(view, message, null, null);
    }

    public static void displaySnackbar(View view,
                                       String message,
                                       @Nullable String actionText,
                                       @Nullable View.OnClickListener onClickListener) {

        displaySnackbar(view, message, actionText, onClickListener, 0);
    }

    public static void displaySnackbar(View view,
                                       String message,
                                       @Nullable String actionText,
                                       @Nullable View.OnClickListener onClickListener,
                                       int durationInMillis) {

        if (view == null)
            return;

        Context context = view.getContext();

        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);

        if (durationInMillis > 0) {

            snackbar.setDuration(durationInMillis);
        }

        View viewSnackbar = snackbar.getView();

        viewSnackbar.setBackgroundColor(ContextCompat.getColor(App.appContext, android.R.color.black));

        TextView textSnackbar = viewSnackbar.findViewById(android.support.design.R.id.snackbar_text);

        textSnackbar.setMaxLines(5);

        textSnackbar.setMinLines(2);

        textSnackbar.setGravity(Gravity.CENTER_VERTICAL);

        textSnackbar.setTextColor(ContextCompat.getColor(context, android.R.color.white));

        if (actionText != null && onClickListener != null) {

            snackbar.setActionTextColor(ContextCompat.getColor(context, R.color.colorAccent));

            snackbar.setAction(actionText, onClickListener);

            snackbar.setDuration(Snackbar.LENGTH_INDEFINITE);
        }

        snackbar.show();
    }

    public static Drawable getRoundedDrawable(Context context, int cornerRadius) {

        GradientDrawable roundedDrawable = new GradientDrawable();

        roundedDrawable.setColor(Color.WHITE);

        roundedDrawable.setCornerRadius(convertDpToPx(cornerRadius, context));

        return roundedDrawable;
    }

    public static ColorStateList getColorStateList(int colorEnabled, int colorPressed, int colorDisabled) {

        int[][] states = new int[][]{
                new int[]{-android.R.attr.state_enabled}, // disabled
                new int[]{android.R.attr.state_pressed}, // pressed
                new int[]{android.R.attr.state_enabled} // enabled
        };

        int[] colors = new int[]{
                colorDisabled, // disabled
                colorPressed, // pressed
                colorEnabled // enabled
        };

        return new ColorStateList(states, colors);
    }

    public static int darkenColor(int color, double fraction) {

        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);

        red = darkenColorChannel(red, fraction);
        green = darkenColorChannel(green, fraction);
        blue = darkenColorChannel(blue, fraction);

        int alpha = Color.alpha(color);

        return Color.argb(alpha, red, green, blue);
    }

    private static int darkenColorChannel(int color, double fraction) {

        return (int) Math.max(color - (color * fraction), 0);
    }

    public static float convertDpToPx(float dp, Context context) {

        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }
}
