package com.edreams.ui.util;

import android.support.v4.util.ArrayMap;
import android.support.v4.util.ArraySet;
import android.text.TextUtils;

import com.edreams.util.SharedPrefs;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FlightListFiltersUtils {

    public static class Constants {

        public static class Labels {

            public static final String ORIGIN = "ORIGIN";
            public static final String DESTINATION = "DESTINATION";
            public static final String PRICE_FROM = "PRICE_FROM";
            public static final String PRICE_TO = "PRICE_TO";
        }
    }

    public static void clearFilters() {

        saveSelectedFiltersValues(new ArrayMap<String, String>());
    }

    public static boolean hasFilters() {

        return !getSelectedFiltersValues().isEmpty();
    }

    public static Map<String, String> getSelectedFiltersValues() {

        return SharedPrefs.INSTANCE.loadMap(
                new TypeReference<Map<String, String>>() {
                },
                SharedPrefs.Constants.FLIGHT_LIST
        );
    }

    public static void saveSelectedFiltersValues(Map<String, String> filtersValues) {

        if (filtersValues == null) {

            filtersValues = new HashMap<>();
        }

        Set<String> keySet = new ArraySet<>(filtersValues.keySet());

        for (String key : keySet) {

            if (TextUtils.isEmpty(filtersValues.get(key))) {

                filtersValues.remove(key);
            }
        }

        SharedPrefs.INSTANCE.saveJsonObject(SharedPrefs.Constants.FLIGHT_LIST, filtersValues);
    }
}
