package com.edreams.ui.util;

import android.text.TextUtils;
import android.util.Log;

import com.edreams.R;
import com.edreams.application.App;
import com.edreams.backend.response.exception.BackendException;
import com.edreams.backend.response.exception.FrontendException;
import com.edreams.backend.response.exception.ServerNotAvailableException;
import com.edreams.backend.util.ConnectionMonitor;
import com.edreams.config.Constants;

public class ErrorUtils {

    public static String getErrorMsg(Throwable e) {

        String errorMessage;

        if (e instanceof BackendException || e instanceof ServerNotAvailableException) {

            errorMessage = e.getMessage();

            Log.e(Constants.APP_TAG, "BackendException - " + errorMessage);

        } else if (e instanceof FrontendException) {

            errorMessage = App.appContext.getString(R.string.default_error);

            Log.e(Constants.APP_TAG, "FrontendException - " + errorMessage);

        } else {

            boolean isOnline = ConnectionMonitor.INSTANCE.isOnline();

            errorMessage = isOnline
                    ? App.appContext.getString(R.string.default_error)
                    : App.appContext.getString(R.string.network_error_fail);

            Log.e(Constants.APP_TAG, "OtherException - " + errorMessage);
        }

        if (TextUtils.isEmpty(errorMessage)) {

            errorMessage = App.appContext.getString(R.string.default_error);

            Log.e(Constants.APP_TAG, "EmptyErrorMessage - " + errorMessage);
        }

        return errorMessage;

    }

}
