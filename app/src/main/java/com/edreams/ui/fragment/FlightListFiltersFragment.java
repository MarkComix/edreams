package com.edreams.ui.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.edreams.R;
import com.edreams.ui.activity.FlightListActivity;
import com.edreams.ui.presenter.FlightListFiltersPresenter;
import com.edreams.ui.util.DecimalDigitsInputFilter;
import com.edreams.ui.util.ViewUtils;
import com.edreams.ui.view.FlightListFiltersView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FlightListFiltersFragment extends BaseFragment<FlightListFiltersPresenter> implements FlightListFiltersView {

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.fragment_flight_list_filters_origin)
    public TextView textViewOrigin;

    @BindView(R.id.fragment_flight_list_filters_destination)
    public TextView textViewDestination;

    @BindView(R.id.fragment_flight_list_filters_from_price)
    public TextView textViewPriceFrom;

    @BindView(R.id.fragment_flight_list_filters_to_price)
    public TextView textViewPriceTo;

    @BindView(R.id.fragment_flight_list_filters_apply)
    public AppCompatButton buttonApply;

    private boolean fromStart;

    public static FlightListFiltersFragment newInstance(boolean fromStart) {

        FlightListFiltersFragment fragment = new FlightListFiltersFragment();

        Bundle args = new Bundle();

        args.putBoolean("EXTRA_FROM_STAR", fromStart);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        this.fromStart = getArguments().getBoolean("EXTRA_FROM_STAR", false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list_flight_filters, container, false);

        this.unbinder = ButterKnife.bind(this, view);

        this.presenter = new FlightListFiltersPresenter();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        this.presenter.setView(this, this);

        initView();

        this.presenter.initialise(fromStart);
    }

    private void initView() {

        if (this.fromStart)
            this.toolbar.setNavigationIcon(null);

        this.buttonApply.setBackground(ViewUtils.getRoundedDrawable(getContext(), 5));

        int colorEnabled = getResources().getColor(R.color.colorPrimary);

        int colorPressed = ViewUtils.darkenColor(colorEnabled, 0.2);

        int colorDisabled = Color.parseColor("#d2d2d2");

        ViewCompat.setBackgroundTintList(
                this.buttonApply,
                ViewUtils.getColorStateList(
                        colorEnabled,
                        colorPressed,
                        colorDisabled
                )
        );

        this.textViewPriceFrom.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(9,2)});
        this.textViewPriceTo.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(9,2)});
    }

    @Override
    public void loadFilters(String origin, String destination, String priceFrom, String priceTo) {

        this.textViewOrigin.setText(origin);
        this.textViewDestination.setText(destination);
        this.textViewPriceFrom.setText(priceFrom);
        this.textViewPriceTo.setText(priceTo);
    }

    @Override
    public void showMessage(String message) {

        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.toolbar)
    void onToolbarClick(){

        getActivity().setResult(Activity.RESULT_CANCELED);

        getActivity().finish();
    }

    @OnClick(R.id.fragment_flight_list_filters_apply)
    void onApplyClick(){

        String origin = this.textViewOrigin.getText().toString();
        String destination = this.textViewDestination.getText().toString();
        String priceFrom = this.textViewPriceFrom.getText().toString();
        String priceTo = this.textViewPriceTo.getText().toString();

        //Is Not mandatory to complete both fields, so, if one of them
        // is empty, I need to complete with 0 the empty one

        if (!TextUtils.isEmpty(priceFrom) && TextUtils.isEmpty(priceTo))
            priceTo = "0";

        if (!TextUtils.isEmpty(priceTo) && TextUtils.isEmpty(priceFrom))
            priceFrom = "0";

        this.presenter.onApplyClick(origin, destination, priceFrom, priceTo);
    }

    @Override
    public void applyFilters() {

        if (this.fromStart)
            getActivity().startActivity(FlightListActivity.getCallingIntent(getActivity()));

        getActivity().setResult(Activity.RESULT_OK);

        getActivity().finish();
    }

    @Override
    public void showPriceFromBiggerPriceToErrorMessage() {

        showMessage(getString(R.string.error_message_price_from_bigger_than_to));
    }

    @Override
    public void showOriginAndDestinationAreRequiredErrorMessage() {

        showMessage(getString(R.string.error_message_origin_and_destination_mandatory));
    }
}
