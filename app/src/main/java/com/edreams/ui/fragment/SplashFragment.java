package com.edreams.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edreams.R;
import com.edreams.application.App;
import com.edreams.ui.activity.FlightListFiltersActivity;
import com.edreams.ui.presenter.SplashPresenter;
import com.edreams.ui.util.ViewUtils;
import com.edreams.ui.view.SplashView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashFragment extends BaseFragment<SplashPresenter> implements SplashView {

    @BindView(R.id.fragment_splash_text_view_version_name)
    TextView textViewVersionName;

    public static SplashFragment newInstance() {

        SplashFragment fragment = new SplashFragment();

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        presenter = new SplashPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_splash, container, false);

        unbinder = ButterKnife.bind(this, view);

        setupVersionName();

        return view;
    }

    private void setupVersionName() {

        String versionNameText = getString(R.string.splash_app_version, App.Companion.versionName());

        textViewVersionName.setText(versionNameText);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        presenter.setView(this, this);

        presenter.initialise();
    }

    @Override
    public void showMessage(String message) {

        ViewUtils.displaySnackbar(getView(), message);
    }

    @Override
    public void navigateToHomeScreen() {

        startActivity(FlightListFiltersActivity.getCallingIntent(getActivity()));

        getActivity().finish();

        getActivity().overridePendingTransition(R.anim.activity_fade_in_fast, R.anim.activity_fade_out_fast);
    }
}
