package com.edreams.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.edreams.ui.presenter.BasePresenter;

import butterknife.Unbinder;

public abstract class BaseFragment<Presenter extends BasePresenter> extends Fragment {

    protected Presenter presenter;

    protected Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {

        super.onResume();

        if (presenter != null) {
            presenter.onResume();
        }
    }

    @Override
    public void onPause() {

        super.onPause();

        if (presenter != null) {
            presenter.onPause();
        }
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();

        if (presenter != null) {
            presenter.onDestroy();
        }

        if (unbinder != null) {

            unbinder.unbind();
        }
    }
}
