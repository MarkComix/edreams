package com.edreams.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edreams.R;
import com.edreams.config.Constants;
import com.edreams.ui.activity.FlightListFiltersActivity;
import com.edreams.ui.adapter.recycler_view.BaseRecyclerViewAdapter;
import com.edreams.ui.adapter.recycler_view.FlightRecyclerViewAdapter;
import com.edreams.ui.presenter.FlightListPresenter;
import com.edreams.ui.view.FlightListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FlightListFragment extends BaseRecyclerViewFragment<FlightListPresenter> implements FlightListView {

    @BindView(R.id.view_add_filters_text_view_filters)
    TextView textViewFilters;

    @BindView(R.id.view_add_filters_image_view_filters_icon)
    ImageView imageViewFiltersIcon;

    public static FlightListFragment newInstance() {

        FlightListFragment fragment = new FlightListFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list_flight, container, false);

        this.unbinder = ButterKnife.bind(this, view);

        this.presenter = new FlightListPresenter();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        this.presenter.setView(this, this);

        this.swipeRefreshLayout.setRefreshing(true);

        this.presenter.initialise();

        this.presenter.getFiltersText();

        this.presenter.refreshList();
    }

    @Override
    protected BaseRecyclerViewAdapter createAdapter() {

        return new FlightRecyclerViewAdapter();
    }

    @Override
    protected void onSwipeRefreshCalled() {

        this.presenter.initialise();

        this.presenter.refreshList();
    }

    @Override
    public void showMessage(String message) {

        this.swipeRefreshLayout.setRefreshing(false);

        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

        this.setEmptyViewVisibility();
    }

    @Override
    public void showList(List list) {

        this.swipeRefreshLayout.setRefreshing(false);

        this.adapter.insertItems(list);

        this.setEmptyViewVisibility();
    }

    private void setEmptyViewVisibility() {

        if (this.adapter == null)
            return;

        this.recyclerView.setVisibility(this.adapter.isEmpty() ? View.GONE : View.VISIBLE);

        this.viewEmpty.setVisibility(this.adapter.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setFilterButtonAndText(String text, boolean showResetButton) {

        this.textViewFilters.setText(text);

        if (showResetButton) {

            this.imageViewFiltersIcon.setRotation(0);

        } else {

            this.imageViewFiltersIcon.setRotation(45);
        }
    }

    @OnClick(R.id.fragment_list_flight_filters)
    void onFiltersClick(){

        this.presenter.onFiltersClick();
    }

    @Override
    public void navigateToFiltersScreen() {

        getActivity().startActivityForResult(FlightListFiltersActivity.getCallingIntent(getActivity()),
                Constants.ActivityForResult.FLIGHT_LIST_FILTERS);
    }

    public void applyFilters() {

        this.swipeRefreshLayout.setRefreshing(true);

        this.presenter.getFiltersText();

        this.presenter.applyFilters();
    }
}
