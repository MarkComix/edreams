package com.edreams.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.edreams.R;
import com.edreams.ui.adapter.recycler_view.BaseRecyclerViewAdapter;
import com.edreams.ui.presenter.BaseRecyclerViewPresenter;

import butterknife.BindView;

public class BaseRecyclerViewFragment<Presenter extends BaseRecyclerViewPresenter> extends BaseFragment<Presenter> {

    @BindView(R.id.view_recycle_view_list)
    RecyclerView recyclerView;

    @BindView(R.id.view_recycle_view_empty_layout)
    protected View viewEmpty;

    @BindView(R.id.view_recycle_view_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    protected BaseRecyclerViewAdapter adapter;

    protected LinearLayoutManager layoutManager;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        setupRecyclerView();
    }

    protected void setupRecyclerView() {

        adapter = this.createAdapter();

        this.setLayoutManager(adapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

               onSwipeRefreshCalled();
            }
        });
    }

    //Must be override on child
    protected BaseRecyclerViewAdapter createAdapter() {

        return null;
    }

    protected void setLayoutManager(BaseRecyclerViewAdapter adapter) {

        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);
    }

    //Must be override on child
    protected void onSwipeRefreshCalled() {

    }

}
