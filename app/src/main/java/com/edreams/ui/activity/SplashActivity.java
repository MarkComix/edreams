package com.edreams.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.edreams.ui.fragment.SplashFragment;

public class SplashActivity extends BaseActivity {

    public static Intent getCallingIntent(Context context) {

        Intent intent = new Intent(context, SplashActivity.class);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {

            replaceFragment(SplashFragment.newInstance());
        }

        if (getSupportActionBar() != null) {

            getSupportActionBar().hide();
        }
    }
}
