package com.edreams.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;

import com.edreams.R;
import com.edreams.ui.fragment.FlightListFiltersFragment;
import com.edreams.ui.util.ViewUtils;

public class FlightListFiltersActivity extends BaseActivity {

    private Boolean exit = false;

    public static Intent getCallingIntent(Context context) {

        Intent intent = new Intent(context, FlightListFiltersActivity.class);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {

            replaceFragment(FlightListFiltersFragment.newInstance(getCallingActivity() == null));
        }

        if (getSupportActionBar() != null) {

            getSupportActionBar().hide();
        }
    }

    /**
     * Closes the Application if the user taps twice on the Back button.
     */
    @Override
    public void onBackPressed() {

        if (exit || getCallingActivity() != null) {

            super.onBackPressed();

        } else {

            View targetView = findViewById(android.R.id.content);

            ViewUtils.displaySnackbar(targetView, getString(R.string.home_screen_exit_app));

            exit = true;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }
}
