package com.edreams.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.Window;

import com.edreams.R;
import com.edreams.config.Constants;
import com.edreams.ui.fragment.FlightListFragment;
import com.edreams.ui.util.ViewUtils;

public class FlightListActivity extends BaseActivity {

    private Boolean exit = false;

    public static Intent getCallingIntent(Context context) {

        Intent intent = new Intent(context, FlightListActivity.class);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {

            replaceFragment(FlightListFragment.newInstance());
        }

        if (getSupportActionBar() != null) {

            getSupportActionBar().hide();
        }
    }

    /**
     * Closes the Application if the user taps twice on the Back button.
     */
    @Override
    public void onBackPressed() {

        if (exit) {

            super.onBackPressed();

        } else {

            View targetView = findViewById(android.R.id.content);

            ViewUtils.displaySnackbar(targetView, getString(R.string.home_screen_exit_app));

            exit = true;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.ActivityForResult.FLIGHT_LIST_FILTERS) {

            if (resultCode == Activity.RESULT_OK) {

                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.activity_base_fragment_container);

                if (fragment instanceof FlightListFragment) {

                    ((FlightListFragment) fragment).applyFilters();
                }
            }
        }
    }
}
