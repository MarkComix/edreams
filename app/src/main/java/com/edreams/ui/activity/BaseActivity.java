package com.edreams.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.edreams.R;
import com.edreams.ui.fragment.BaseFragment;
import com.edreams.ui.presenter.BasePresenter;

public abstract class BaseActivity<Presenter extends BasePresenter> extends AppCompatActivity {

    protected Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
    }

    public void replaceFragment(BaseFragment fragment) {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_base_fragment_container, fragment)
                .commit();
    }

    @Override
    public void onResume() {

        super.onResume();

        if (presenter != null) {
            presenter.onResume();
        }
    }

    @Override
    public void onPause() {

        super.onPause();

        if (presenter != null) {
            presenter.onPause();
        }
    }

    @Override
    public void onDestroy() {

        super.onDestroy();

        if (presenter != null) {
            presenter.onDestroy();
        }
    }

}
