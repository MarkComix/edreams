package com.edreams.util

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.fasterxml.jackson.core.type.TypeReference
import java.io.IOException
import java.util.*

object SharedPrefs {

    private lateinit var preferences: SharedPreferences

    object Constants {

        const val FLIGHT_LIST = "FLIGHT_LIST"
    }

    fun initialise(context: Context) {

        preferences = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    }

    fun containsPreference(key: String): Boolean {

        return preferences.contains(key)
    }

    fun clearPreferences() {

        preferences.edit().clear().apply()
    }

    fun removeElement(key: String) {

        if (containsPreference(key))
            preferences.edit().remove(key).apply()
    }

    fun loadString(key: String, defValue: String): String? {

        return preferences.getString(key, defValue)
    }

    fun saveString(key: String, value: String) {

        preferences.edit().putString(key, value).apply()
    }

    fun loadInt(key: String, defValue: Int): Int {

        return preferences.getInt(key, defValue)
    }

    fun saveInt(key: String, value: Int) {

        preferences.edit().putInt(key, value).apply()
    }

    fun loadLong(key: String, defValue: Long): Long {

        return preferences.getLong(key, defValue)
    }

    fun saveLong(key: String, value: Long) {

        preferences.edit().putLong(key, value).apply()
    }

    fun loadFloat(key: String, defValue: Float): Float {

        return preferences.getFloat(key, defValue)
    }

    fun saveFloat(key: String, value: Float) {

        preferences.edit().putFloat(key, value).apply()
    }

    fun loadBoolean(key: String, defValue: Boolean): Boolean {

        return preferences.getBoolean(key, defValue)
    }

    fun saveBoolean(key: String, value: Boolean) {

        preferences.edit().putBoolean(key, value).apply()
    }

    fun <T, U> loadMap(typeRef: TypeReference<Map<T, U>>, key: String): Map<T, U> {

        val json = loadString(key, "")

        if (!TextUtils.isEmpty(json)) {

            try {

                var map: Map<T, U>? = BaseFunctions.getMapper().readValue<Map<T, U>>(json, typeRef)

                if (map == null) {

                    map = HashMap()
                }

                return map

            } catch (e: IOException) {

                e.printStackTrace()
            }
        }

        return HashMap()
    }

    fun saveJsonObject(key: String, `object`: Any) {

        val json = BaseFunctions.convertObjectToJson(`object`)

        saveString(key, json)
    }
}
