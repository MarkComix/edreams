package com.edreams.util;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class BaseFunctions {

    private static ObjectMapper mapper;

    static {

        mapper = new ObjectMapper();

        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        DateFormat WS_DATE_FORMAT = new SimpleDateFormat("MMM dd, yyyy h:mm:ss aa", Locale.ENGLISH);

        mapper.setDateFormat(WS_DATE_FORMAT);
    }

    private BaseFunctions() {
        // to prevent instantiation
    }

    public static ObjectMapper getMapper() {

        return mapper;
    }


    public static String convertObjectToJson(Object obj) {

        try {

            return mapper.writeValueAsString(obj);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static <T> T getObjectFromJson(String json, Class<T> target) {

        try {

            return (T) mapper.readValue(json, Class.forName(target.getName()));

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
