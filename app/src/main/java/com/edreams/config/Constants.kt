package com.edreams.config

object Constants {

    const val APP_TAG = "eDreamsTest"

    object ResponseStatus {

        const val STATUS_OK = "OK"
        const val STATUS_FAIL = "FAIL"
        const val STATUS_MAINTENANCE = "MAINTENANCE"

    }

    object CoreKeys {

        const val GET_FLIGHT_LIST = "GET_FLIGHT_LIST"
        const val SAVE_FLIGHT_LIST = "SAVE_FLIGHT_LIST"
        const val GET_CURRENCY = "GET_CURRENCY"
        const val GET_FLIGHT_CURRENCY_LIST = "GET_FLIGHT_CURRENCY_LIST"
    }

    object ActivityForResult {

        const val FLIGHT_LIST_FILTERS = 11
    }

    object Params {

        const val ORIGIN = "ORIGIN"
        const val DESTINATION = "DESTINATION"
        const val PRICE_FROM = "PRICE_FROM"
        const val PRICE_TO = "PRICE_TO"

        const val CURRENCY_FROM = "CURRENCY_FROM"
        const val CURRENCY_TO = "CURRENCY_TO"

        const val FLIGHT_LIST = "FLIGHT_LIST"
    }

    object Endpoint {

        const val GET_FLIGHT_LIST = "/"
        const val GET_CURRENCY = "/currency"
    }

    object CurrencySymbols {

        const val EUR = "EUR"
    }
}