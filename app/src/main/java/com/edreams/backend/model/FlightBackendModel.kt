package com.edreams.backend.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class FlightBackendModel(var inbound: FlightDataBackendModel = FlightDataBackendModel(),
                              var outbound: FlightDataBackendModel = FlightDataBackendModel(),
                              var price: String = "0",
                              var currency: String = "") : BaseBackendModel() {

    init {

    }
}
