package com.edreams.backend.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class FlightDataBackendModel(var airline: String = "",
                                  var airlineImage: String = "",
                                  var destination: String = "",
                                  var origin: String = "") : BaseBackendModel() {

    init {

    }
}
