package com.edreams.backend.model

data class CurrencyBackendModel(var currency: String = "",
                                var exchangeRate: String = "0") : BaseBackendModel() {

    init {

    }
}
