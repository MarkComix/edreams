package com.edreams.backend.resolver


import com.edreams.backend.response.GetFlightListResponse


class GetFlightListBackendResolver : BaseBackendResolver<GetFlightListResponse>() {

    override fun getDataFromBackend() {

        client.flightList().enqueue(this)
    }
}