package com.edreams.backend.resolver


import com.edreams.backend.response.GetCurrencyResponse
import com.edreams.config.Constants


class GetCurrencyBackendResolver : BaseBackendResolver<GetCurrencyResponse>() {

    override fun getDataFromBackend() {

        val url = "https://jarvisstark.herokuapp.com" + Constants.Endpoint.GET_CURRENCY

        val currencyFrom = params[Constants.Params.CURRENCY_FROM] as String

        val currencyTo = params[Constants.Params.CURRENCY_TO] as String

        client.getCurrency(url, currencyFrom, currencyTo).enqueue(this)
    }
}