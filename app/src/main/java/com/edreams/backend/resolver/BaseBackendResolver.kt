package com.edreams.backend.resolver

import android.text.TextUtils
import com.edreams.backend.ApiService
import com.edreams.backend.response.BaseResponse
import com.edreams.backend.response.exception.BackendException
import com.edreams.backend.response.exception.ServerNotAvailableException
import com.edreams.backend.util.ConnectionMonitor
import com.edreams.backend.util.RestClient
import com.edreams.util.BaseFunctions
import com.fasterxml.jackson.core.type.TypeReference
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import rx.Observable
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import java.util.*

abstract class BaseBackendResolver<T : BaseResponse>(protected var client: ApiService = RestClient.INSTANCE.apiService,
                                                     var params: Map<String, Any> = HashMap()) : Callback<T> {

    protected lateinit var subscriber: Subscriber<in T>

    fun makeCall(): Observable<T> {

        return Observable.create(
                Observable.OnSubscribe<T> { subscriber ->

                    this@BaseBackendResolver.subscriber = subscriber

                    if (ConnectionMonitor.isOnline) {

                        getDataFromBackend()
                    }
                }
        )
        .subscribeOn(AndroidSchedulers.mainThread())
        .observeOn(AndroidSchedulers.mainThread())
    }

    protected abstract fun getDataFromBackend()

    override fun onResponse(call: Call<T>, response: Response<T>) {

        val baseResponse: T?

        val statusCode = response.code()

        try {

            if (statusCode in 200..203) {

                baseResponse = response.body()

                this.onSuccessResponse(baseResponse)

                subscriber.onNext(baseResponse)

                subscriber.onCompleted()

            } else {

                val jsonResponse = response.errorBody()!!.string()

                baseResponse = BaseFunctions.getMapper().readValue<T>(
                        jsonResponse,
                        object : TypeReference<T>() { }
                )

                this.onFailResponse(baseResponse)

                subscriber.onError(getBackendException(statusCode, baseResponse))
            }

        } catch (e: Exception) {

            this.onFailResponse(getBackendException(statusCode, null))

            subscriber.onError(e)
        }

    }

    override fun onFailure(call: Call<T>, t: Throwable) {

        this.onFailResponse(t)

        subscriber.onError(t)
    }

    private fun onSuccessResponse(baseResponse: BaseResponse?) {
        // to be implemented by subclasses, when necessary
    }

    private fun onFailResponse(baseResponse: BaseResponse?) {
        // to be implemented by subclasses, when necessary
    }

    private fun onFailResponse(t: Throwable) {
        // to be implemented by subclasses, when necessary
    }

    private fun getBackendException(statusCode: Int, response: BaseResponse?): BackendException {

        // the server is down
        if (statusCode >= 500) {

            return ServerNotAvailableException()
        }

        // the backend returned a message for the user
        return if (statusCode == 200 && response != null && response.isResponseFail()
                && !TextUtils.isEmpty(response.message)) {

            BackendException(response.message)

        } else
            BackendException()

    }
}
