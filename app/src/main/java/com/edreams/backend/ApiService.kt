package com.edreams.backend

import com.edreams.backend.response.GetCurrencyResponse
import com.edreams.backend.response.GetFlightListResponse
import com.edreams.config.Constants

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface ApiService {

    @GET(Constants.Endpoint.GET_FLIGHT_LIST)
    fun flightList(
    ): Call<GetFlightListResponse>

    @GET
    fun getCurrency(
            @Url url: String,
            @Query("from") currencyFrom: String,
            @Query("to") currencyTo: String
    ): Call<GetCurrencyResponse>

}
