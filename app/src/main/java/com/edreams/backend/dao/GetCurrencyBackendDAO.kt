package com.edreams.backend.dao

import com.edreams.backend.model.CurrencyBackendModel
import com.edreams.backend.resolver.GetCurrencyBackendResolver
import rx.Observable

object GetCurrencyBackendDAO : IBackendDAO<CurrencyBackendModel> {

    override fun makeCall(params: Map<String, Any>): Observable<CurrencyBackendModel> {

        val resolver = GetCurrencyBackendResolver()

        resolver.params = params

        return resolver.makeCall().map { response -> CurrencyBackendModel(response.currency, response.exchangeRate) }
    }
}
