package com.edreams.backend.dao

import com.edreams.backend.model.FlightBackendModel
import com.edreams.backend.resolver.GetFlightListBackendResolver
import rx.Observable

object GetFlightListBackendDAO : IBackendDAO<List<FlightBackendModel>> {

    override fun makeCall(params: Map<String, Any>): Observable<List<FlightBackendModel>> {

        val resolver = GetFlightListBackendResolver()

        resolver.params = params

        return resolver.makeCall().map { response -> response.results }
    }
}
