package com.edreams.backend.dao

import rx.Observable

interface IBackendDAO<BackendModel> {

    fun makeCall(params: Map<String, Any>): Observable<BackendModel>
}
