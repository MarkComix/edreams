package com.edreams.backend.util

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import com.edreams.application.App
import com.edreams.config.Constants

object ConnectionMonitor {

    private val connectivityManager = App.appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private var connected = false

    val isOnline: Boolean
        get() {

            try {

                val networkInfo = connectivityManager.activeNetworkInfo

                connected = networkInfo != null && networkInfo.isAvailable && networkInfo.isConnected

                return connected

            } catch (e: Exception) {

                Log.e(Constants.APP_TAG, "ConnectionMonitor Exception", e)
            }

            return connected
        }
}
