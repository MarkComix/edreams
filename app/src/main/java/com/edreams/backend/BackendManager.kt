package com.edreams.backend

import com.edreams.backend.dao.GetCurrencyBackendDAO
import com.edreams.backend.dao.GetFlightListBackendDAO
import com.edreams.backend.dao.IBackendDAO
import com.edreams.config.Constants

object BackendManager {

    fun getBackendDAO(coreKey: String): IBackendDAO<*>? {

        return when (coreKey) {

            Constants.CoreKeys.GET_FLIGHT_LIST -> GetFlightListBackendDAO

            Constants.CoreKeys.GET_CURRENCY -> GetCurrencyBackendDAO

            else -> null
        }
    }
}