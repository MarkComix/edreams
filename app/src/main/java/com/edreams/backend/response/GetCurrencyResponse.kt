package com.edreams.backend.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class GetCurrencyResponse(var currency: String = "",
                               var exchangeRate: String = "0") : BaseResponse() {

    init {

    }
}