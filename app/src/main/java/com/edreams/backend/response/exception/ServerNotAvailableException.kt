package com.edreams.backend.response.exception

class ServerNotAvailableException : BackendException()
