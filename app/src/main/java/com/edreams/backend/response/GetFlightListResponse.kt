package com.edreams.backend.response

import com.edreams.backend.model.FlightBackendModel
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class GetFlightListResponse(var results: List<FlightBackendModel>? = ArrayList()) : BaseResponse() {

    init {

    }
}
