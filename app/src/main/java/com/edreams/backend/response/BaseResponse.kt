package com.edreams.backend.response

import com.edreams.config.Constants
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
open class BaseResponse(var code: String = "",
                        var message: String = "") {

    fun isResponseOk(): Boolean {

        return code.equals(Constants.ResponseStatus.STATUS_OK, ignoreCase = true)
    }

    fun isResponseFail(): Boolean {

        return code.equals(Constants.ResponseStatus.STATUS_FAIL, ignoreCase = true)
    }

    fun isResponseMaintenance(): Boolean {

        return code.equals(Constants.ResponseStatus.STATUS_MAINTENANCE, ignoreCase = true)
    }
}
