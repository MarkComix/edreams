package com.edreams.backend.response.exception

import com.edreams.backend.response.BaseResponse

open class BackendException(var reason: Int = 0,
                            var response: BaseResponse? = null,
                            var userReadableMessage: String? = "") : Exception() {

    constructor(response: String) : this() {

        this.userReadableMessage = response;
    }

}
