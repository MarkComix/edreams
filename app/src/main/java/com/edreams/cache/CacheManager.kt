package com.edreams.cache

import com.edreams.cache.dao.GetFlightListCacheDAO
import com.edreams.cache.dao.GetFlightsCurrencyListCacheDAO
import com.edreams.cache.dao.ICacheDAO
import com.edreams.cache.dao.SaveFlightListCacheDAO
import com.edreams.config.Constants

object CacheManager {

    fun getItemDAO(coreKey: String): ICacheDAO<*>? {

        return when (coreKey) {

            Constants.CoreKeys.GET_FLIGHT_LIST -> GetFlightListCacheDAO

            Constants.CoreKeys.SAVE_FLIGHT_LIST -> SaveFlightListCacheDAO

            Constants.CoreKeys.GET_FLIGHT_CURRENCY_LIST -> GetFlightsCurrencyListCacheDAO

            else -> null
        }
    }
}
