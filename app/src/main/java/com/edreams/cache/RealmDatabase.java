package com.edreams.cache;

import com.edreams.application.App;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmDatabase {

    private static RealmDatabase INSTANCE;

    private static final int DB_VERSION = 1;

    private Realm realm;

    private RealmDatabase() {

        Realm.init(App.appContext);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(DB_VERSION) // Migrate to the new version(if required).
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);

        realm = Realm.getInstance(config);
    }

    public static RealmDatabase getInstance() {

        if (INSTANCE == null) {

            INSTANCE = new RealmDatabase();
        }

        return INSTANCE;
    }

    public Realm getRealm() {

        return realm;
    }
}