package com.edreams.cache.model;

import com.edreams.backend.model.FlightBackendModel;
import com.edreams.business.model.FlightBusinessModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;

public class FlightRealmModel extends RealmObject {

    private FlightDataRealmModel inbound;
    private FlightDataRealmModel outbound;
    private float price;
    private String currency;

    public FlightRealmModel() {

        this.inbound = new FlightDataRealmModel();
        this.outbound = new FlightDataRealmModel();
        this.price = 0;
        this.currency = "";
    }

    public FlightRealmModel(FlightBusinessModel businessModel) {

        this.inbound = new FlightDataRealmModel(businessModel.getInbound());
        this.outbound = new FlightDataRealmModel(businessModel.getOutbound());
        this.price = businessModel.getPrice();
        this.currency = businessModel.getCurrency();
    }

    public FlightRealmModel(FlightBackendModel backendModel) {

        this.inbound = new FlightDataRealmModel(backendModel.getInbound());
        this.outbound = new FlightDataRealmModel(backendModel.getOutbound());
        this.price = Float.parseFloat(backendModel.getPrice());
        this.currency = backendModel.getCurrency();
    }

    public static List<FlightRealmModel> fromBusinessModel(List<FlightBusinessModel> businessModelList) {

        List<FlightRealmModel> realmModelList = new ArrayList<>();

        if (businessModelList == null)
            return realmModelList;

        for (FlightBusinessModel businessModel : businessModelList) {

            realmModelList.add(new FlightRealmModel(businessModel));
        }

        return realmModelList;
    }

    public static List<FlightRealmModel> fromBackendModelList(List<FlightBackendModel> backendModelList) {

        List<FlightRealmModel> realmModelList = new ArrayList<>();

        if (backendModelList == null)
            return realmModelList;

        for (FlightBackendModel businessModel : backendModelList) {

            realmModelList.add(new FlightRealmModel(businessModel));
        }

        return realmModelList;
    }

    public FlightDataRealmModel getInbound() {
        return inbound;
    }

    public FlightDataRealmModel getOutbound() {
        return outbound;
    }

    public float getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }
}
