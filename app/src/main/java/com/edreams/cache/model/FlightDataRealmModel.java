package com.edreams.cache.model;

import com.edreams.backend.model.FlightDataBackendModel;
import com.edreams.business.model.FlightDataBusinessModel;

import io.realm.RealmObject;

public class FlightDataRealmModel extends RealmObject {

    private String airline;
    private String airlineImage;
    private String destination;
    private String origin;

    public FlightDataRealmModel() {

        this.airline = "";
        this.airlineImage = "";
        this.destination = "";
        this.origin = "";
    }

    public FlightDataRealmModel(FlightDataBackendModel backendModel) {

        super();

        this.airline = backendModel.getAirline();
        this.airlineImage = backendModel.getAirlineImage();
        this.destination = backendModel.getDestination();
        this.origin = backendModel.getOrigin();
    }

    public FlightDataRealmModel(FlightDataBusinessModel businessModel) {

        super();

        this.airline = businessModel.getAirline();
        this.airlineImage = businessModel.getAirlineImage();
        this.destination = businessModel.getDestination();
        this.origin = businessModel.getOrigin();
    }

    public String getAirline() {
        return airline;
    }

    public String getAirlineImage() {
        return airlineImage;
    }

    public String getDestination() {
        return destination;
    }

    public String getOrigin() {
        return origin;
    }
}
