package com.edreams.cache.model;

import io.realm.RealmObject;

public class BooleanRealmModel extends RealmObject {

    private Boolean result;

    public BooleanRealmModel() {

        this.result = false;
    }

    public BooleanRealmModel(boolean result) {

        this.result = result;
    }

    public Boolean getResult() {

        return result;
    }
}
