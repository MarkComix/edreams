package com.edreams.cache.util

import android.text.TextUtils
import com.edreams.cache.model.FlightRealmModel

import com.edreams.config.Constants

import io.realm.Case
import io.realm.RealmQuery

object QueryGenerator {

    fun getFlightGroup(params: Map<String, Any>, query: RealmQuery<FlightRealmModel>): RealmQuery<FlightRealmModel> {

        var query = query

        var origin = ""

        if (params.containsKey(Constants.Params.ORIGIN))
            origin = params[Constants.Params.ORIGIN] as String

        var destination = ""

        if (params.containsKey(Constants.Params.DESTINATION))
            destination = params[Constants.Params.DESTINATION] as String

        if (!params.isEmpty()) {

            query = query.beginGroup()

            if (!TextUtils.isEmpty(origin)) {

                query = query.equalTo("inbound.origin", origin, Case.INSENSITIVE)
            }

            if (!TextUtils.isEmpty(destination)) {

                query = query.equalTo("inbound.destination", destination, Case.INSENSITIVE)
            }

            query = query.endGroup()
        }

        return query
    }
}
