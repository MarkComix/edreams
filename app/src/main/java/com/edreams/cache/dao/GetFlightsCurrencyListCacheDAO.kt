package com.edreams.cache.dao

import com.edreams.cache.model.FlightRealmModel
import com.edreams.cache.resolver.GetFlightsCurrencyListRealmResolver

import rx.Observable

object GetFlightsCurrencyListCacheDAO : ICacheDAO<List<FlightRealmModel>> {

    override fun excecuteQuery(params: Map<String, Any>): Observable<List<FlightRealmModel>> {

        val resolver = GetFlightsCurrencyListRealmResolver()

        return resolver.executeQuery(params)
    }
}
