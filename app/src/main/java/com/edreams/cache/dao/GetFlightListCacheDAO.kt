package com.edreams.cache.dao

import com.edreams.cache.model.FlightRealmModel
import com.edreams.cache.resolver.GetFlightListRealmResolver

import rx.Observable

object GetFlightListCacheDAO : ICacheDAO<List<FlightRealmModel>> {

    override fun excecuteQuery(params: Map<String, Any>): Observable<List<FlightRealmModel>> {

        val resolver = GetFlightListRealmResolver()

        return resolver.executeQuery(params)
    }
}
