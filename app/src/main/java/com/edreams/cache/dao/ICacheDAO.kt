package com.edreams.cache.dao

import rx.Observable

interface ICacheDAO<RealmObject> {

    fun excecuteQuery(params: Map<String, Any>): Observable<RealmObject>
}
