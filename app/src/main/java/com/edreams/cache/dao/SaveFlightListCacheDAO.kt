package com.edreams.cache.dao

import com.edreams.cache.model.BooleanRealmModel
import com.edreams.cache.resolver.SaveFlightListRealmResolver

import rx.Observable

object SaveFlightListCacheDAO : ICacheDAO<BooleanRealmModel> {

    override fun excecuteQuery(params: Map<String, Any>): Observable<BooleanRealmModel> {

        val resolver = SaveFlightListRealmResolver()

        return resolver.executeQuery(params)
    }
}
