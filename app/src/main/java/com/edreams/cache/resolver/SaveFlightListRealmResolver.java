package com.edreams.cache.resolver;

import com.edreams.cache.model.BooleanRealmModel;
import com.edreams.cache.model.FlightRealmModel;
import com.edreams.config.Constants;

import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

public class SaveFlightListRealmResolver extends BaseRealmResolver {

    @SuppressWarnings("unchecked")
    public Observable<BooleanRealmModel> executeQuery(final Map<String, Object> params) {

        return Observable.create(

                new Observable.OnSubscribe<BooleanRealmModel>() {

                    @Override
                    public void call(final Subscriber<? super BooleanRealmModel> subscriber) {

                        List<FlightRealmModel> list = (List<FlightRealmModel>) params.get(Constants.Params.FLIGHT_LIST);

                        realm.beginTransaction();

                        realm.deleteAll();

                        realm.copyToRealmOrUpdate(list);

                        realm.commitTransaction();

                        subscriber.onNext(new BooleanRealmModel(true));

                        subscriber.onCompleted();
                    }
                }
        )
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
