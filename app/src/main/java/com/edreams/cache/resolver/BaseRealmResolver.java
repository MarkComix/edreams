package com.edreams.cache.resolver;

import com.edreams.cache.RealmDatabase;

import io.realm.Realm;

public class BaseRealmResolver {

    protected Realm realm;

    public BaseRealmResolver() {

        this.realm = RealmDatabase.getInstance().getRealm();
    }
}