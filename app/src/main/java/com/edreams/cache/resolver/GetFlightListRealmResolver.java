package com.edreams.cache.resolver;

import com.edreams.cache.model.FlightRealmModel;
import com.edreams.cache.util.QueryGenerator;

import java.util.List;
import java.util.Map;

import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

public class GetFlightListRealmResolver extends BaseRealmResolver{

    @SuppressWarnings("unchecked")
    public Observable<List<FlightRealmModel>> executeQuery(final Map<String, Object> params) {

        return Observable.create(
                new Observable.OnSubscribe<List<FlightRealmModel>>() {

                    @Override
                    public void call(final Subscriber<? super List<FlightRealmModel>> subscriber) {

                        String[] sortFieldNames = {"price"};
                        Sort[] sortOrders = {Sort.ASCENDING};

                        RealmQuery<FlightRealmModel> query = realm.where(FlightRealmModel.class);

                        query = QueryGenerator.INSTANCE.getFlightGroup(params, query);

                        RealmResults<FlightRealmModel> items = query.findAllSorted(sortFieldNames, sortOrders);

                        subscriber.onNext(items);

                        subscriber.onCompleted();
                    }

                }
        )
        .subscribeOn(AndroidSchedulers.mainThread())
        .observeOn(AndroidSchedulers.mainThread());
    }
}
