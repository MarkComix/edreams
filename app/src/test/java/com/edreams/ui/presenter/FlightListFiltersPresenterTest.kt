package com.edreams.ui.presenter

import com.edreams.ui.fragment.FlightListFiltersFragment
import com.edreams.ui.view.FlightListFiltersView
import org.junit.Test
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FlightListFiltersPresenterTest {

    private lateinit var presenter: FlightListFiltersPresenter

    @Mock
    private lateinit var mockedFragment: FlightListFiltersFragment

    @Mock
    private lateinit var mockedView: FlightListFiltersView

    @Before
    fun setupTest() {

        presenter = FlightListFiltersPresenter()

        presenter.setView(mockedView, mockedFragment)
    }

    @Test
    fun testOriginAndDestinationAreRequired() {

        val origin = ""
        val destination = "Old Ghis"

        presenter.onApplyClick(origin, destination, "", "")

        verify(mockedView).showOriginAndDestinationAreRequiredErrorMessage()
    }

    @Test
    fun testPriceFromBiggerPriceTo() {

        val origin = "Lorath"
        val destination = "Old Ghis"

        val priceFrom = "100"
        val priceTo = "50"

        presenter.onApplyClick(origin, destination, priceFrom, priceTo)

        verify(mockedView).showPriceFromBiggerPriceToErrorMessage()
    }

    @Test
    fun onApplyClick() {
    }
}